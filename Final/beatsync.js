var bpmText = document.getElementById('bpmtext');
var noteDuration = 0;

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext('2d');

// establishing offline and regular audio contexts
var offlineAudioCtx;
var sourceURL = './snd/Kalimba.mp3';
var audioCtx;
var offlineSource;
var source;
var filter;
var updateImageIndexTimer = 0;
var onBeatHitTimer = 0;
var numBeatsHit = 0;

var analyser;
var freqData;
var songOffsetTime;

var xhr = new XMLHttpRequest();
xhr.open('GET', sourceURL, true);
xhr.responseType = 'arraybuffer';
xhr.onload = function(){
  console.log("Song Has Loaded.");
  offlineAudioCtx = new OfflineAudioContext(1, 44100 * 100, 44100);
  offlineAudioCtx.decodeAudioData(xhr.response, function(buffer){
    offlineSource = offlineAudioCtx.createBufferSource();
    try{
      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      audioCtx = new AudioContext();
    } catch(e){
      console.log("Error. Try a different browser.");
    }

    analyser = audioCtx.createAnalyser();
    analyser.fftSize = 256;
    freqData = new Uint8Array(128);

    source = audioCtx.createBufferSource();
    //source.loop = true;
    filter = offlineAudioCtx.createBiquadFilter();
    filter.type = "lowpass";

    offlineSource.buffer = buffer;
    source.buffer = buffer;

    offlineSource.connect(filter);
    filter.connect(offlineAudioCtx.destination);
    source.connect(analyser);
    analyser.connect(audioCtx.destination);

    offlineSource.start(0);
    source.start(0);
    numBeatsHit = 0;
    songOffsetTime = audioCtx.currentTime;

    offlineAudioCtx.startRendering();

    offlineAudioCtx.oncomplete = function(e) {
      var filteredBuffer = e.renderedBuffer;

      var peaks = getPeaksAtThreshold(filteredBuffer.getChannelData(0), .9);
      var intervalsWithCounts = countIntervalsBetweenNearbyPeaks(peaks);
      var tempoCounts = groupNeighborsByTempo(intervalsWithCounts);
      var tempo = determineTempo(tempoCounts);
      tempo = Math.round(tempo);

      bpmText.innerHTML = "BPM: " + tempo;

      noteDuration = 60 / tempo; //seconds per beat
      noteDuration = noteDuration * 1000; //ms per beat

      clearTimeout(updateImageIndexTimer);
      clearTimeout(onBeatHitTimer);

      setTimeout(onBeatHit, noteDuration);
      imageIndex = 0;
      setTimeout(UpdateImageIndex, noteDuration / 16);
    };
  }, function(e){console.log("Error loading Song");});
}
xhr.send();

var flashColor = "black"
var flashLineWidth = 1;
var dancingImg = new Image();
dancingImg.src = './img/GameCube - Donkey Konga 2 - Donkey Kong.png';

function onBeatHit() {
  flashColor = "blue";
  imageIndex = 0;
  flashLineWidth = 5;
  setTimeout(function(){ flashColor = "black"; flashLineWidth = 1; }, 40);
  //onBeatHitTimer = setTimeout(onBeatHit, noteDuration);
}

audio_file.onchange = function() {
  var files = this.files;
  var file = URL.createObjectURL(files[0]);

  source.stop();

  xhr.open('GET', file, true);
  xhr.send();
}

var imageIndex = 0;

function UpdateImageIndex() {
  imageIndex++;
  if (imageIndex == 10) imageIndex = 9;
  //updateImageIndexTimer = setTimeout(UpdateImageIndex, noteDuration / 16);
}

var barheights = [];
for (var i = 0; i < 128; i++) {
  barheights[i] = 0;
};

var Render = function(){
  canvas.width = 800;
  var tileWidth = 20;
  for (var i = 0; i < 11; i++){
      ctx.beginPath();
      ctx.strokeStyle = flashColor;
      ctx.lineWidth = flashLineWidth;
      ctx.moveTo(0, i * tileWidth);
      ctx.lineTo(canvas.width, i * tileWidth);
      ctx.stroke();
  }

  if (analyser){
    analyser.getByteFrequencyData(freqData);
    for (var i = 0; i < 128; i++) {
      barheights[i] = (100 * freqData[i] / 255);
      ctx.beginPath();
      ctx.strokeStyle = 'orange';
      var barWidth = 6.25;
      ctx.lineWidth = barWidth;
      ctx.moveTo(i * barWidth, 100 - barheights[i]);
      ctx.lineTo(i * barWidth, 100 + barheights[i]);
      ctx.stroke();
    };

    if (noteDuration > 0){
      var playbackTime = (audioCtx.currentTime - songOffsetTime) * 1000;
      var thisNumBeatsHit = 0;
      while (playbackTime > noteDuration){
        playbackTime -= noteDuration;
        thisNumBeatsHit++;
      }
      if (thisNumBeatsHit > numBeatsHit){
        onBeatHit();
        numBeatsHit = thisNumBeatsHit;
      }
      var beatRatio = playbackTime / noteDuration;
      imageIndex = Math.floor(beatRatio * 16);

      ctx.beginPath();
      ctx.strokeStyle = 'black';
      ctx.lineWidth = 10;
      ctx.moveTo(0, 100);
      ctx.lineTo(800 - (800 * beatRatio), 100);
      ctx.stroke();
    }
  }

  ctx.save();
  ctx.translate(400, 100);
  ctx.drawImage(dancingImg, imageIndex * 160, 140, 160, 140, -80, -70, 160, 140);
  ctx.restore();

  window.requestAnimationFrame(Render);
}
Render();

// BPM detection code inspired by: http://tech.beatport.com/2014/web-audio/beat-detection-using-web-audio/

function getPeaksAtThreshold(data, threshold) {
  var peaksArray = [];
  var length = data.length;
  for(var i = 0; i < length;) {
    if (data[i] > threshold) {
      peaksArray.push(i);
      i += 10000;
    }
    i++;
  }
  return peaksArray;
}

function countIntervalsBetweenNearbyPeaks(peaks) {
  var intervalCounts = [];
  peaks.forEach(function(peak, index) {
    for(var i = 0; i < 10; i++) {
      var interval = peaks[index + i] - peak;
      var foundInterval = intervalCounts.some(function(intervalCount) {
        if (intervalCount.interval === interval)
          return intervalCount.count++;
      });
      if (!foundInterval) {
        intervalCounts.push({
          interval: interval,
          count: 1
        });
      }
    }
  });
  return intervalCounts;
}

function groupNeighborsByTempo(intervalCounts) {
  var tempoCounts = []
  intervalCounts.forEach(function(intervalCount, i) {
    // Convert an interval to tempo
    if (intervalCount.interval > 0){
      var theoreticalTempo = 60 / (intervalCount.interval / 44100 );

      // Adjust the tempo to fit within the 90-180 BPM range
      while (theoreticalTempo < 90) theoreticalTempo *= 2;
      while (theoreticalTempo > 180) theoreticalTempo /= 2;

      var foundTempo = tempoCounts.some(function(tempoCount) {
        if (tempoCount.tempo === theoreticalTempo)
          return tempoCount.count += intervalCount.count;
      });
      if (!foundTempo) {
        tempoCounts.push({
          tempo: theoreticalTempo,
          count: intervalCount.count
        });
      }
    }
  });

  return tempoCounts;
}

function determineTempo(tempoCounts) {
  var highestTempoCount = 0;
  var tempoMode;
  tempoCounts.forEach(function(tempoCount, i){
    if (tempoCount.count > highestTempoCount) {
      highestTempoCount = tempoCount.count;
      tempoMode = tempoCount.tempo;
    }
  });

  return tempoMode;
}

window.addEventListener("keydown", function(e){
  if (String.fromCharCode(e.keyCode) == "S"){
    if (audioCtx){
      songOffsetTime = audioCtx.currentTime;
      numBeatsHit = 0;
    }
  }
}, false);