function pollGamepads(){
	var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
	for (var i = 0; i < gamepads.length; i++) {
		var thisGamepad = gamepads[i];
		if (thisGamepad){
			gamepadLoop(i);
		}
	};
}

function buttonPressed(button){
	if (typeof(button) == "object"){
		return button.pressed;
	}
	return button == 1.0;
}

function gamepadLoop(gamepadIndex){
	var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
	if (!gamepads)
		return;
	var player = gamepads[gamepadIndex];

	var AXIS_THRESHOLD = 0.3;
	var PLAYER_SPEED = 3.0;
	var PLAYER_ROT_SPEED = .05;
	if (Math.abs(player.axes[0]) > AXIS_THRESHOLD){
		
	}
	if (Math.abs(player.axes[1]) > AXIS_THRESHOLD){
		
	}
}

var okSound = document.createElement('audio');
var okSource = document.createElement('source');
okSource.type = 'audio/mpeg';
okSource.src = 'https://raw.githubusercontent.com/GameTheoryLabs/GameTheoryLabs.github.io/master/psl/sounds/ding-happy.wav';
okSound.appendChild(okSource);
document.body.appendChild(okSound);

var clickSound = document.createElement('audio');
var clickSource = document.createElement('source');
clickSource.type = 'audio/mpeg';
clickSource.src = 'https://raw.githubusercontent.com/GameTheoryLabs/GameTheoryLabs.github.io/master/psl/sounds/button-click.wav';
clickSound.appendChild(clickSource);
document.body.appendChild(clickSound);

var backSound = document.createElement('audio');
var backSource = document.createElement('source');
backSource.type = 'audio/mpeg';
backSource.src = 'https://raw.githubusercontent.com/GameTheoryLabs/GameTheoryLabs.github.io/master/psl/sounds/ding-sad.wav';
backSound.appendChild(backSource);
document.body.appendChild(backSound);

var errorSound = document.createElement('audio');
var errorSource = document.createElement('source');
errorSource.type = 'audio/mpeg';
errorSource.src = 'http://gametheorylabs.com/JaHOVAOS/scripts/Applications/Audio/Chirp.mp3';
errorSound.appendChild(errorSource);
document.body.appendChild(errorSound);

var isKeyDown = [];
for (var i = 0; i < 128; i++) {
	isKeyDown[i] = false;
};
var fullscreen = false;
window.addEventListener("keydown", function(e){

	if (String.fromCharCode(e.keyCode) == "A"){
		if (state == 'options'){
			if (subMenuSelectedBoxIndex == subMenuStartIndex){
				infectedSourceGain.gain.value -= 0.02;
				if (infectedSourceGain.gain.value < 0)
					infectedSourceGain.gain.value = 0;
				clickSound.currentTime = 0;
				//clickSound.play();
			}
		}
	}

	if (String.fromCharCode(e.keyCode) == "D"){
		if (state == 'options'){
			if (subMenuSelectedBoxIndex == subMenuStartIndex){
				infectedSourceGain.gain.value += 0.02;
				if (infectedSourceGain.gain.value > 1)
					infectedSourceGain.gain.value = 1;
				clickSound.currentTime = 0;
				//clickSound.play();
			}
		}
	}

	if (isKeyDown[e.keyCode] == false){
		if (String.fromCharCode(e.keyCode) == "F"){
			if (!fullscreen){
				renderer.domElement.requestFullscreen();
				renderer.domElement.requestPointerLock();
				fullscreen = true;
			} else {
				document.exitFullscreen();
				document.exitPointerLock();
				fullscreen = false;
			}
		}

		if (String.fromCharCode(e.keyCode) == "W"){
			if (state == 'mainMenu'){
				if (mainMenuSelectedBoxIndex > mainMenuStartIndex){
					mainMenuSelectedBoxIndex--;
					clickSound.currentTime = 0;
					clickSound.play();
				}
			} else {
				if (subMenuSelectedBoxIndex > subMenuStartIndex){
					subMenuSelectedBoxIndex--;
					clickSound.currentTime = 0;
					clickSound.play();
				}
			}
		}
		if (String.fromCharCode(e.keyCode) == "S"){
			if (state == 'mainMenu'){
				if (mainMenuSelectedBoxIndex < mainMenuStartIndex + 4){
					mainMenuSelectedBoxIndex++;
					clickSound.currentTime = 0;
					clickSound.play();
				}
			} else {
				if (subMenuSelectedBoxIndex < subMenuStartIndex + numSubMenuOptions - 1){
					subMenuSelectedBoxIndex++;
					clickSound.currentTime = 0;
					clickSound.play();
				}
			}
		}
		if (e.keyCode == 32 || e.keyCode == 69){ //space bar
			if (state == 'mainMenu' && movingMenuDepth == 0){
				if (mainMenuSelectedBoxIndex == mainMenuStartIndex){
					state = 'newGame';
					menuDepth = 1;
					numSubMenuOptions = 3;
					okSound.currentTime = 0;
					okSound.play();
				} else if (mainMenuSelectedBoxIndex == mainMenuStartIndex + 1){
					if (loadGameActive){
						state = 'loadGame';
						menuDepth = 1;
						numSubMenuOptions = 3;
						okSound.currentTime = 0;
						okSound.play();
					} else {
						errorSound.currentTime = 0;
						errorSound.play();
						errorShake = 0.01;
					}
				} else if (mainMenuSelectedBoxIndex == mainMenuStartIndex + 2){
					state = 'options';
					menuDepth = 1;
					numSubMenuOptions = 1;
					okSound.currentTime = 0;
					okSound.play();
				} else if (mainMenuSelectedBoxIndex == mainMenuStartIndex + 3){
					errorSound.currentTime = 0;
					errorSound.play();
					errorShake = 0.01;
				} else if (mainMenuSelectedBoxIndex == mainMenuStartIndex + 4){
					state = 'credits';
					menuDepth = 1;
					numSubMenuOptions = 2;
					okSound.currentTime = 0;
					okSound.play();
				}
			} else if (state == 'newGame'){
				okSound.currentTime = 0;
				okSound.play();
				if (subMenuSelectedBoxIndex == subMenuStartIndex)
					game1Exists = true;
				if (subMenuSelectedBoxIndex == subMenuStartIndex + 1)
					game2Exists = true;
				if (subMenuSelectedBoxIndex == subMenuStartIndex + 2)
					game3Exists = true;
			} else if (state == 'loadGame'){
				if (subMenuSelectedBoxIndex == subMenuStartIndex){
					if (game1Exists){
						okSound.currentTime = 0;
						okSound.play();
					} else {
						errorSound.currentTime = 0;
						errorSound.play();
						errorShake = 0.01;
					}
				}
				if (subMenuSelectedBoxIndex == subMenuStartIndex + 1){
					if (game2Exists){
						okSound.currentTime = 0;
						okSound.play();
					} else {
						errorSound.currentTime = 0;
						errorSound.play();
						errorShake = 0.01;
					}
				}
				if (subMenuSelectedBoxIndex == subMenuStartIndex + 2){
					if (game3Exists){
						okSound.currentTime = 0;
						okSound.play();
					} else {
						errorSound.currentTime = 0;
						errorSound.play();
						errorShake = 0.01;
					}
				}
			}
		}
		if (e.keyCode == 81){ //can't use backspace(8), so use Q instead?
			if (menuDepth == 1 && movingMenuDepth == 1){
				state = 'mainMenu';
				menuDepth = 0;
				subMenuSelectedBoxIndex = subMenuStartIndex;
				backSound.currentTime = 0;
				backSound.play();
			}
		}
	}

	isKeyDown[e.keyCode] = true;
}, false);

window.addEventListener("keyup", function(e){
	isKeyDown[e.keyCode] = false;
}, false);


var infectedURL = './snd/06 Gorgeous George.mp3';//'https://dl.dropboxusercontent.com/u/8942221/Infected.mp3';

var audioCtx;

try{
	window.AudioContext = window.AudioContext || window.webkitAudioContext;
	audioCtx = new AudioContext();
} catch(e){
	console.log("Error. Try a different browser.");
}

var infectedAudio = null;
var infectedSource = audioCtx.createBufferSource();
infectedSource.loop = true;
var infectedSourceGain = audioCtx.createGain();
infectedSourceGain.gain.value = .5;

var xhr = new XMLHttpRequest();
xhr.open('GET', infectedURL, true);
xhr.responseType = 'arraybuffer';
xhr.onload = function(){
	console.log("Infected Has Loaded.");
	audioCtx.decodeAudioData(xhr.response, function(buffer){
		infectedAudio = buffer;
		infectedSource.buffer = infectedAudio;
		infectedSource.connect(infectedSourceGain);
		infectedSourceGain.connect(audioCtx.destination);
		infectedSource.start(0);
	}, function(e){console.log("Error loading Infected");});
}
xhr.send();

function IsKeyDown(character){
	return isKeyDown[character.charCodeAt(0)];
}

var deltaSeconds = 1.0/60.0;
var menuScrollSpeed = 5.0;
var mainMenuStartIndex = 4;
var subMenuStartIndex = 4;
var state = 'mainMenu';
var menuDepth = 0;
var movingMenuDepth = 0;
var movingMenuDepthLinear = 0;
var menuSideScrollSpeed = 2;
var numSubMenuOptions = 3;
var game1Exists = false;
var game2Exists = false;
var game3Exists = false;
var loadGameActive = false;
var errorShake = 0;
var errorShakeOffset = 0;

var Update = function(){
	pollGamepads();

	if (errorShake > 0){
		errorShake += deltaSeconds * 50;
		errorShakeOffset = Math.sin(errorShake)/errorShake;
		if (errorShake > 10){
			errorShake = 0;
		}
	} else {
		errorShakeOffset = 0;
	}

	var slowdownFactor = 10;
	if (mainMenuSelectedBoxIndex > mainMenuPosition){
		var distanceScale = mainMenuSelectedBoxIndex - Math.floor(mainMenuPosition);
		mainMenuPosition = ((mainMenuPosition * (slowdownFactor - 1)) + mainMenuSelectedBoxIndex) / slowdownFactor;
		if (mainMenuPosition > mainMenuSelectedBoxIndex)
			mainMenuPosition = mainMenuSelectedBoxIndex;
	} else if (mainMenuSelectedBoxIndex < mainMenuPosition){
		var distanceScale = Math.ceil(mainMenuPosition) - mainMenuSelectedBoxIndex;
		mainMenuPosition = ((mainMenuPosition * (slowdownFactor - 1)) + mainMenuSelectedBoxIndex) / slowdownFactor;
		if (mainMenuPosition < mainMenuSelectedBoxIndex)
			mainMenuPosition = mainMenuSelectedBoxIndex;
	}

	if (subMenuSelectedBoxIndex > subMenuPosition){
		var distanceScale = subMenuSelectedBoxIndex - Math.floor(subMenuPosition);
		subMenuPosition = ((subMenuPosition * (slowdownFactor - 1)) + subMenuSelectedBoxIndex) / slowdownFactor;
		if (subMenuPosition > subMenuSelectedBoxIndex)
			subMenuPosition = subMenuSelectedBoxIndex;
	} else if (subMenuSelectedBoxIndex < subMenuPosition){
		var distanceScale = Math.ceil(subMenuPosition) - subMenuSelectedBoxIndex;
		subMenuPosition = ((subMenuPosition * (slowdownFactor - 1)) + subMenuSelectedBoxIndex) / slowdownFactor;
		if (subMenuPosition < subMenuSelectedBoxIndex)
			subMenuPosition = subMenuSelectedBoxIndex;
	}

	if (menuDepth > movingMenuDepthLinear){
		movingMenuDepthLinear += menuSideScrollSpeed * deltaSeconds;
		if (movingMenuDepthLinear > menuDepth)
			movingMenuDepthLinear = menuDepth;
		movingMenuDepth = Math.pow(movingMenuDepthLinear, 1/3);
	} else if (menuDepth < movingMenuDepthLinear){
		movingMenuDepthLinear -= menuSideScrollSpeed * deltaSeconds;
		if (movingMenuDepthLinear < menuDepth)
			movingMenuDepthLinear = menuDepth;
		movingMenuDepth = movingMenuDepthLinear * movingMenuDepthLinear * movingMenuDepthLinear;
	}

	var textSidePadding = 0.2;

	for (var i = 0; i < 16; i++) {
		mainMenuCubes[i].position.y = (mainMenuPosition + mainMenuPosition * heightBetweenBoxes) + (-i - (heightBetweenBoxes * i)) + errorShakeOffset;
		subMenuCubes[i].position.y = (subMenuPosition + subMenuPosition * heightBetweenBoxes) + (-i - (heightBetweenBoxes * i)) + errorShakeOffset;
		var mainDistFromTop = Math.abs(mainMenuPosition - i) / 5;
		var subDistFromTop = Math.abs(subMenuPosition - i) / 5;

		var mainScaleValue = 1.0 - mainDistFromTop;
		var mainScaleValuePow4 = mainScaleValue * mainScaleValue * mainScaleValue * mainScaleValue;
		var subScaleValue = 1.0 - subDistFromTop;
		var subScaleValuePow4 = subScaleValue * subScaleValue * subScaleValue * subScaleValue;

		if (i >= mainMenuStartIndex && i < mainMenuStartIndex + 5){
			mainMenuCubes[i].scale.x = 8 * mainScaleValuePow4 + 5;
		} else {
			mainMenuCubes[i].scale.x = 5 * mainScaleValuePow4 + 1;
		}
		if (i >= subMenuStartIndex && i < subMenuStartIndex + numSubMenuOptions){
			subMenuCubes[i].scale.x = 8 * subScaleValuePow4 + 5;
		} else {
			subMenuCubes[i].scale.x = 5 * subScaleValuePow4 + 1;
		}

		mainMenuCubes[i].position.x = 5 + (mainDistFromTop * mainDistFromTop * mainDistFromTop * 5) - (mainMenuCubes[i].scale.x * .5);
		mainMenuCubes[i].position.x -= 10 * movingMenuDepth;
		subMenuCubes[i].position.x = 35 + (subDistFromTop * subDistFromTop * subDistFromTop * 5) - (subMenuCubes[i].scale.x * .5);
		subMenuCubes[i].position.x -= 25 * movingMenuDepth;

		var menuDepthColorScale = 1.0 - 0.5 * movingMenuDepth;
		if (mainMenuSelectedBoxIndex == i)
				menuDepthColorScale = 1.0;

		//RIGHT
		mainMenuCubes[i].geometry.faces[0].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		mainMenuCubes[i].geometry.faces[1].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		//LEFT
		mainMenuCubes[i].geometry.faces[2].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		mainMenuCubes[i].geometry.faces[3].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		//TOP
		mainMenuCubes[i].geometry.faces[4].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		mainMenuCubes[i].geometry.faces[5].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		//BOTTOM
		mainMenuCubes[i].geometry.faces[6].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		mainMenuCubes[i].geometry.faces[7].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		//FRONT
		mainMenuCubes[i].geometry.faces[8].color.setRGB(0.5 * mainScaleValue * menuDepthColorScale, 0.375 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		mainMenuCubes[i].geometry.faces[9].color.setRGB(0.5 * mainScaleValue * menuDepthColorScale, 0.375 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);

		//LEFT
		subMenuCubes[i].geometry.faces[2].color.setRGB(1.0 * subScaleValue, 1.0 * subScaleValue, 1.0 * subScaleValue);
		subMenuCubes[i].geometry.faces[3].color.setRGB(1.0 * subScaleValue, 1.0 * subScaleValue, 1.0 * subScaleValue);
		//TOP
		subMenuCubes[i].geometry.faces[4].color.setRGB(1.0 * subScaleValue, 1.0 * subScaleValue, 1.0 * subScaleValue);
		subMenuCubes[i].geometry.faces[5].color.setRGB(1.0 * subScaleValue, 1.0 * subScaleValue, 1.0 * subScaleValue);
		//BOTTOM
		subMenuCubes[i].geometry.faces[6].color.setRGB(1.0 * subScaleValue, 1.0 * subScaleValue, 1.0 * subScaleValue);
		subMenuCubes[i].geometry.faces[7].color.setRGB(1.0 * subScaleValue, 1.0 * subScaleValue, 1.0 * subScaleValue);
		//FRONT
		subMenuCubes[i].geometry.faces[8].color.setRGB(1.0 * subScaleValue, 0.375 * subScaleValue, 0.5 * subScaleValue);
		subMenuCubes[i].geometry.faces[9].color.setRGB(1.0 * subScaleValue, 0.375 * subScaleValue, 0.5 * subScaleValue);


		mainMenuCubes[i].geometry.colorsNeedUpdate = true;
		subMenuCubes[i].geometry.colorsNeedUpdate = true;

		if (i == mainMenuStartIndex){
			newGameTextMesh.material.materials[0].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		} else if (i == mainMenuStartIndex + 1){
			if (loadGameActive)
				loadGameTextMesh.material.materials[0].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
			else
				loadGameTextMesh.material.materials[0].color.setRGB(0.6 * mainScaleValue * menuDepthColorScale, 0.6 * mainScaleValue * menuDepthColorScale, 0.6 * mainScaleValue * menuDepthColorScale);
		} else if (i == mainMenuStartIndex + 2){
			optionsTextMesh.material.materials[0].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		} else if (i == mainMenuStartIndex + 3){
			extrasTextMesh.material.materials[0].color.setRGB(0.6 * mainScaleValue * menuDepthColorScale, 0.6 * mainScaleValue * menuDepthColorScale, 0.6 * mainScaleValue * menuDepthColorScale);
		} else if (i == mainMenuStartIndex + 4){
			creditsTextMesh.material.materials[0].color.setRGB(1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale, 1.0 * mainScaleValue * menuDepthColorScale);
		}

		var slot1Color = 1.0;
		var slot2Color = 1.0;
		var slot3Color = 1.0;
		if (mainMenuSelectedBoxIndex == mainMenuStartIndex + 1){ //load game selected
			if (!game1Exists) slot1Color = 0.6;
			if (!game2Exists) slot2Color = 0.6;
			if (!game3Exists) slot3Color = 0.6;
		}

		loadGameActive = game1Exists || game2Exists || game3Exists;

		if (i == subMenuStartIndex){
			slot1TextMesh.material.materials[0].color.setRGB(slot1Color * subScaleValue, slot1Color * subScaleValue, slot1Color * subScaleValue);
			everythingTextMesh.material.materials[0].color.setRGB(1.0 * subScaleValue, 1.0 * subScaleValue, 1.0 * subScaleValue);
			volumeTextMesh.material.materials[0].color.setRGB(1.0 * subScaleValue, 1.0 * subScaleValue, 1.0 * subScaleValue);
		} else if (i == subMenuStartIndex + 1){
			slot2TextMesh.material.materials[0].color.setRGB(slot2Color * subScaleValue, slot2Color * subScaleValue, slot2Color * subScaleValue);
			toolsTextMesh.material.materials[0].color.setRGB(1.0 * subScaleValue, 1.0 * subScaleValue, 1.0 * subScaleValue);
		} else if (i == subMenuStartIndex + 2){
			slot3TextMesh.material.materials[0].color.setRGB(slot3Color * subScaleValue, slot3Color * subScaleValue, slot3Color * subScaleValue);
		}
	}

	newGameTextMesh.position.x = mainMenuCubes[mainMenuStartIndex].position.x + 0.5 * mainMenuCubes[mainMenuStartIndex].scale.x - newGameText.textWidth - textSidePadding;
	newGameTextMesh.position.y = mainMenuCubes[mainMenuStartIndex].position.y - 0.4;

	loadGameTextMesh.position.x = mainMenuCubes[mainMenuStartIndex + 1].position.x + 0.5 * mainMenuCubes[mainMenuStartIndex + 1].scale.x - loadGameText.textWidth - textSidePadding;
	loadGameTextMesh.position.y = mainMenuCubes[mainMenuStartIndex + 1].position.y - 0.4;

	optionsTextMesh.position.x = mainMenuCubes[mainMenuStartIndex + 2].position.x + 0.5 * mainMenuCubes[mainMenuStartIndex + 2].scale.x - optionsText.textWidth - textSidePadding;
	optionsTextMesh.position.y = mainMenuCubes[mainMenuStartIndex + 2].position.y - 0.4;

	extrasTextMesh.position.x = mainMenuCubes[mainMenuStartIndex + 3].position.x + 0.5 * mainMenuCubes[mainMenuStartIndex + 3].scale.x - extrasText.textWidth - textSidePadding;
	extrasTextMesh.position.y = mainMenuCubes[mainMenuStartIndex + 3].position.y - 0.4;

	creditsTextMesh.position.x = mainMenuCubes[mainMenuStartIndex + 4].position.x + 0.5 * mainMenuCubes[mainMenuStartIndex + 4].scale.x - creditsText.textWidth - textSidePadding;
	creditsTextMesh.position.y = mainMenuCubes[mainMenuStartIndex + 4].position.y - 0.4;

	if (mainMenuSelectedBoxIndex < mainMenuStartIndex + 2){
		slot1TextMesh.position.x = subMenuCubes[subMenuStartIndex].position.x + 0.5 * subMenuCubes[subMenuStartIndex].scale.x - slot1Text.textWidth - textSidePadding;
		slot1TextMesh.position.y = subMenuCubes[subMenuStartIndex].position.y - 0.4;

		slot2TextMesh.position.x = subMenuCubes[subMenuStartIndex + 1].position.x + 0.5 * subMenuCubes[subMenuStartIndex + 1].scale.x - slot2Text.textWidth - textSidePadding;
		slot2TextMesh.position.y = subMenuCubes[subMenuStartIndex + 1].position.y - 0.4;

		slot3TextMesh.position.x = subMenuCubes[subMenuStartIndex + 2].position.x + 0.5 * subMenuCubes[subMenuStartIndex + 2].scale.x - slot3Text.textWidth - textSidePadding;
		slot3TextMesh.position.y = subMenuCubes[subMenuStartIndex + 2].position.y - 0.4;
	} else {
		slot1TextMesh.position.x = 50;
		slot1TextMesh.position.y = 0;

		slot2TextMesh.position.x = 50;
		slot2TextMesh.position.y = 0;

		slot3TextMesh.position.x = 50;
		slot3TextMesh.position.y = 0;
	}

	if (mainMenuSelectedBoxIndex == mainMenuStartIndex + 2){
		if (subMenuSelectedBoxIndex == subMenuStartIndex){
			if (volumeLine.material.opacity < 1){
				volumeLine.material.opacity += .1;
				volumeSlider.material.opacity += .1;
			}
		} else {
			if (volumeLine.material.opacity > 0){
				volumeLine.material.opacity -= .1;
				volumeSlider.material.opacity -= .1;
			}
		}

		volumeTextMesh.position.x = subMenuCubes[subMenuStartIndex].position.x + 0.5 * subMenuCubes[subMenuStartIndex].scale.x - volumeText.textWidth - textSidePadding;
		volumeTextMesh.position.y = subMenuCubes[subMenuStartIndex].position.y - 0.4;

		volumeLine.position.x = subMenuCubes[subMenuStartIndex].position.x - 0.15 * subMenuCubes[subMenuStartIndex].scale.x;
		volumeLine.position.y = subMenuCubes[subMenuStartIndex].position.y - 0.1;

		volumeSlider.position.x = subMenuCubes[subMenuStartIndex].position.x - 0.15 * subMenuCubes[subMenuStartIndex].scale.x;
		volumeSlider.position.x -= 0.5 * volumeLine.scale.x;
		volumeSlider.position.x += infectedSourceGain.gain.value * volumeLine.scale.x;
		volumeSlider.position.y = subMenuCubes[subMenuStartIndex].position.y - 0.1;
	} else {
		volumeTextMesh.position.x = 50;
		volumeTextMesh.position.y = 0;

		volumeLine.position.x = 50;
		volumeLine.position.y = 0;

		volumeSlider.position.x = 50;
		volumeSlider.position.y = 0;
	}

	if (mainMenuSelectedBoxIndex == mainMenuStartIndex + 4){
		if (subMenuSelectedBoxIndex == subMenuStartIndex){
			if (trevorTextMesh.material.materials[0].opacity < 1)
				trevorTextMesh.material.materials[0].opacity += .1;
		} else {
			if (trevorTextMesh.material.materials[0].opacity > 0)
				trevorTextMesh.material.materials[0].opacity -= .1;
		}

		if (subMenuSelectedBoxIndex == subMenuStartIndex + 1){
			if (threejsTextMesh.material.materials[0].opacity < 1)
				threejsTextMesh.material.materials[0].opacity += .1;
		} else {
			if (threejsTextMesh.material.materials[0].opacity > 0)
				threejsTextMesh.material.materials[0].opacity -= .1;
		}

		everythingTextMesh.position.x = subMenuCubes[subMenuStartIndex].position.x - 0.5 * subMenuCubes[subMenuStartIndex].scale.x + textSidePadding;
		everythingTextMesh.position.y = subMenuCubes[subMenuStartIndex].position.y - 0.4;

		trevorTextMesh.position.x = subMenuCubes[subMenuStartIndex].position.x + 0.5 * subMenuCubes[subMenuStartIndex].scale.x - trevorText.textWidth - textSidePadding;
		trevorTextMesh.position.y = subMenuCubes[subMenuStartIndex].position.y - 0.4;

		toolsTextMesh.position.x = subMenuCubes[subMenuStartIndex + 1].position.x - 0.5 * subMenuCubes[subMenuStartIndex + 1].scale.x + textSidePadding;
		toolsTextMesh.position.y = subMenuCubes[subMenuStartIndex + 1].position.y - 0.4;

		threejsTextMesh.position.x = subMenuCubes[subMenuStartIndex + 1].position.x + 0.5 * subMenuCubes[subMenuStartIndex + 1].scale.x - threejsText.textWidth - textSidePadding;
		threejsTextMesh.position.y = subMenuCubes[subMenuStartIndex + 1].position.y - 0.4;
	} else {
		everythingTextMesh.position.x = 50;
		everythingTextMesh.position.y = 0;

		toolsTextMesh.position.x = 50;
		toolsTextMesh.position.y = 0;
	}

	window.requestAnimationFrame(Update);
}


var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

renderer.domElement.requestPointerLock = renderer.domElement.requestPointerLock || renderer.domElement.mozRequestPointerLock || renderer.domElement.webkitRequestPointerLock;
document.exitPointerLock = document.exitPointerLock || document.mozExitPointerLock || document.webkitExitPointerLock;
document.addEventListener('webkitpointerlockchange', function(){console.log("Pointer lock changed")});

renderer.domElement.requestFullscreen = renderer.domElement.requestFullscreen || renderer.domElement.mozRequestFullscreen || renderer.domElement.webkitRequestFullscreen;
document.exitFullscreen = document.exitFullscreen || document.mozExitFullscreen || document.webkitExitFullscreen;
document.addEventListener('webkitfullscreenchange', function(){console.log("Fullscreen changed")});
document.fullscreenEnabled = document.fullscreenEnabled || document.mozFullscreenEnabled || document.webkitFullscreenEnabled;


//var geometry = new THREE.BoxGeometry( 1, 1, 1 );

var mainMenuSelectedBoxIndex = mainMenuStartIndex;
var mainMenuPosition = mainMenuStartIndex;
var subMenuSelectedBoxIndex = subMenuStartIndex;
var subMenuPosition = subMenuStartIndex;
var heightBetweenBoxes = 0.5;

var mainMenuCubes = [];
var subMenuCubes = [];
for (var i = 0; i < 16; i++) {
	var mainMenuCube = new THREE.Mesh( new THREE.BoxGeometry( 1, 1, 1 ), new THREE.MeshBasicMaterial( { color: 0xffffff, vertexColors: THREE.FaceColors } ) );
	mainMenuCube.scale.z = 0.3;
	mainMenuCubes.push(mainMenuCube);
	scene.add( mainMenuCube );

	var subMenuCube = new THREE.Mesh( new THREE.BoxGeometry( 1, 1, 1 ), new THREE.MeshBasicMaterial( { color: 0xffffff, vertexColors: THREE.FaceColors } ) );
	subMenuCube.scale.z = 0.3;
	subMenuCubes.push(subMenuCube);
	scene.add( subMenuCube );
}

var volumeLineMaxWidth = 8;
var volumeLine = new THREE.Mesh( new THREE.BoxGeometry( 1, 1, 1 ), new THREE.MeshBasicMaterial( { color: 0x000000 } ) );
volumeLine.scale.x = volumeLineMaxWidth;
volumeLine.scale.y = .1;
volumeLine.scale.z = .31;
scene.add(volumeLine);
volumeLine.material.transparent = true;
volumeLine.material.opacity = 0;

var volumeSlider = new THREE.Mesh( new THREE.BoxGeometry( 1, 1, 1 ), new THREE.MeshBasicMaterial( { color: 0xffffff } ) );
volumeSlider.scale.x = .2;
volumeSlider.scale.y = .6;
volumeSlider.scale.z = .32;
scene.add(volumeSlider);
volumeSlider.material.transparent = true;
volumeSlider.material.opacity = 0;

var fontSize = 0.8;
var fontHeight = 0;
var fontZPosition = 0.2;
var smallTextScale = 0.8;

var textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
var textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
var textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var newGameTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var newGameText = new THREE.TextGeometry( 'New Game', {
	font: 'gentilis',
	size: fontSize,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var newGameTextMesh = new THREE.Mesh( newGameText, newGameTextMaterial );
newGameTextMesh.position.z = fontZPosition;
newGameText.computeBoundingBox();
newGameText.textWidth = newGameText.boundingBox.max.x - newGameText.boundingBox.min.x;
scene.add(newGameTextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var loadGameTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var loadGameText = new THREE.TextGeometry( 'Load Game', {
	font: 'gentilis',
	size: fontSize,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var loadGameTextMesh = new THREE.Mesh( loadGameText, loadGameTextMaterial );
loadGameTextMesh.position.z = fontZPosition;
loadGameText.computeBoundingBox();
loadGameText.textWidth = loadGameText.boundingBox.max.x - loadGameText.boundingBox.min.x;
scene.add(loadGameTextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var optionsTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var optionsText = new THREE.TextGeometry( 'Options', {
	font: 'gentilis',
	size: fontSize,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var optionsTextMesh = new THREE.Mesh( optionsText, optionsTextMaterial );
optionsTextMesh.position.z = fontZPosition;
optionsText.computeBoundingBox();
optionsText.textWidth = optionsText.boundingBox.max.x - optionsText.boundingBox.min.x;
scene.add(optionsTextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var extrasTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var extrasText = new THREE.TextGeometry( 'Extras', {
	font: 'gentilis',
	size: fontSize,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var extrasTextMesh = new THREE.Mesh( extrasText, extrasTextMaterial );
extrasTextMesh.position.z = fontZPosition;
extrasText.computeBoundingBox();
extrasText.textWidth = extrasText.boundingBox.max.x - extrasText.boundingBox.min.x;
scene.add(extrasTextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var creditsTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var creditsText = new THREE.TextGeometry( 'Credits', {
	font: 'gentilis',
	size: fontSize,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var creditsTextMesh = new THREE.Mesh( creditsText, creditsTextMaterial );
creditsTextMesh.position.z = fontZPosition;
creditsText.computeBoundingBox();
creditsText.textWidth = creditsText.boundingBox.max.x - creditsText.boundingBox.min.x;
scene.add(creditsTextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var slot1TextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var slot1Text = new THREE.TextGeometry( 'Slot 1', {
	font: 'gentilis',
	size: fontSize,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var slot1TextMesh = new THREE.Mesh( slot1Text, slot1TextMaterial );
slot1TextMesh.position.z = fontZPosition;
slot1Text.computeBoundingBox();
slot1Text.textWidth = slot1Text.boundingBox.max.x - slot1Text.boundingBox.min.x;
scene.add(slot1TextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var slot2TextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var slot2Text = new THREE.TextGeometry( 'Slot 2', {
	font: 'gentilis',
	size: fontSize,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var slot2TextMesh = new THREE.Mesh( slot2Text, slot2TextMaterial );
slot2TextMesh.position.z = fontZPosition;
slot2Text.computeBoundingBox();
slot2Text.textWidth = slot2Text.boundingBox.max.x - slot2Text.boundingBox.min.x;
scene.add(slot2TextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var slot3TextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var slot3Text = new THREE.TextGeometry( 'Slot 3', {
	font: 'gentilis',
	size: fontSize,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var slot3TextMesh = new THREE.Mesh( slot3Text, slot3TextMaterial );
slot3TextMesh.position.z = fontZPosition;
slot3Text.computeBoundingBox();
slot3Text.textWidth = slot3Text.boundingBox.max.x - slot3Text.boundingBox.min.x;
scene.add(slot3TextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var volumeTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var volumeText = new THREE.TextGeometry( 'Music', {
	font: 'gentilis',
	size: fontSize,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var volumeTextMesh = new THREE.Mesh( volumeText, volumeTextMaterial );
volumeTextMesh.position.z = fontZPosition;
volumeText.computeBoundingBox();
volumeText.textWidth = volumeText.boundingBox.max.x - volumeText.boundingBox.min.x;
scene.add(volumeTextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var toolsTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var toolsText = new THREE.TextGeometry( 'Tools:', {
	font: 'gentilis',
	size: fontSize * smallTextScale,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var toolsTextMesh = new THREE.Mesh( toolsText, toolsTextMaterial );
toolsTextMesh.position.z = fontZPosition;
toolsText.computeBoundingBox();
toolsText.textWidth = toolsText.boundingBox.max.x - toolsText.boundingBox.min.x;
scene.add(toolsTextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var threejsTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var threejsText = new THREE.TextGeometry( 'Three.js', {
	font: 'gentilis',
	size: fontSize * smallTextScale,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var threejsTextMesh = new THREE.Mesh( threejsText, threejsTextMaterial );
threejsTextMesh.position.z = fontZPosition;
threejsText.computeBoundingBox();
threejsText.textWidth = threejsText.boundingBox.max.x - threejsText.boundingBox.min.x;
scene.add(threejsTextMesh);
threejsTextMesh.material.materials[0].opacity = 0;
threejsTextMesh.material.materials[0].transparent = true;

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var everythingTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var everythingText = new THREE.TextGeometry( 'Programming:', {
	font: 'gentilis',
	size: fontSize * smallTextScale,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var everythingTextMesh = new THREE.Mesh( everythingText, everythingTextMaterial );
everythingTextMesh.position.z = fontZPosition;
everythingText.computeBoundingBox();
everythingText.textWidth = everythingText.boundingBox.max.x - everythingText.boundingBox.min.x;
scene.add(everythingTextMesh);

textFaceMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
textExtrudeMaterial = new THREE.MeshBasicMaterial( { color: 0x000000 } );
textMaterialArray = [ textFaceMaterial, textExtrudeMaterial ];
var trevorTextMaterial = new THREE.MeshFaceMaterial(textMaterialArray);
var trevorText = new THREE.TextGeometry( 'Trevor Youngblood', {
	font: 'gentilis',
	size: fontSize * smallTextScale,
	height: fontHeight,
	material: 0,
	extrudeMaterial: 1
} );
var trevorTextMesh = new THREE.Mesh( trevorText, trevorTextMaterial );
trevorTextMesh.position.z = fontZPosition;
trevorText.computeBoundingBox();
trevorText.textWidth = trevorText.boundingBox.max.x - trevorText.boundingBox.min.x;
scene.add(trevorTextMesh);
trevorTextMesh.material.materials[0].opacity = 0;
trevorTextMesh.material.materials[0].transparent = true;

Update();

camera.position.z = 10;

var render = function () {
	requestAnimationFrame( render );

	renderer.render(scene, camera);
};
render();